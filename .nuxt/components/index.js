export { default as Menu } from '../../components/Menu.vue'
export { default as Narrative } from '../../components/Narrative.vue'

export const LazyMenu = import('../../components/Menu.vue' /* webpackChunkName: "components/Menu" */).then(c => c.default || c)
export const LazyNarrative = import('../../components/Narrative.vue' /* webpackChunkName: "components/Narrative" */).then(c => c.default || c)
